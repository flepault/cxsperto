package com.lhs.ccb.sperto.oci.interceptor;

import com.lhs.ccb.sperto.oci.utils.HttpServletRequestUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HttpRequestInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) throws Exception {

        if (request.getParameter("SuStepName") != null) {
            HttpServletRequestUtils.getWorkflowRequestInformationAndStore(
                    request, request.getParameter("SuStepName"));
        }
        return true;
    }
}