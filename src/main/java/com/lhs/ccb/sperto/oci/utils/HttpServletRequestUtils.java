package com.lhs.ccb.sperto.oci.utils;

import com.lhs.ccb.cfw.cda.utility.UserPropertiesFacade;
import com.lhs.ccb.sperto.oci.controller.HomePageOCIController;
import com.lhs.ccb.sperto.oci.enumeration.HttpHeader;
import com.lhs.ccb.sperto.oci.jdbc.dao.CheckUserIpDao;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HttpServletRequestUtils {

    protected static final Logger logger = Logger.getLogger(HomePageOCIController.class.getName());

    public static String getLoginRequestInformationAndStore(HttpServletRequest request) {

        String userName = UserPropertiesFacade.instance().getUserName();
        String IP = HttpServletRequestUtils.getRemoteIpFrom(request);
        String sessionId = UserPropertiesFacade.instance().getSessionId();
        String hostname = HttpServletRequestUtils.getHostname(request, IP);
        String remoteUser = request.getRemoteUser();

        try {
            CheckUserIpDao.getInstance().insertLoginTrace(IP, hostname, userName, sessionId, remoteUser);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
        }

        return userName;


    }

    public static String getWorkflowRequestInformationAndStore(HttpServletRequest request, String workflowStep) {
        String userName = UserPropertiesFacade.instance().getUserName();
        String IP = HttpServletRequestUtils.getRemoteIpFrom(request);
        String sessionId = UserPropertiesFacade.instance().getSessionId();
        String hostname = HttpServletRequestUtils.getHostname(request, IP);
        String remoteUser = request.getRemoteUser();

        try {
            CheckUserIpDao.getInstance().insertUserTrace(IP, hostname, userName, sessionId, workflowStep, remoteUser);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
        }

        return userName;
    }

    public static String getHostname(HttpServletRequest request, String IP) {
        InetAddress addr = null;

        try {
            addr = InetAddress.getByName(IP);
        } catch (UnknownHostException var10) {
            var10.printStackTrace();
        }

        String hostname;
        if (addr == null) {
            hostname = request.getRemoteHost();
        } else {
            hostname = addr.getHostName();
        }
        return hostname;
    }

    public static String getRemoteIpFrom(HttpServletRequest request) {
        String ip = null;

        for (int tryCount = 1; !isIpFound(ip) && tryCount <= 6; ++tryCount) {
            switch (tryCount) {
                case 1:
                    ip = request.getHeader(HttpHeader.X_FORWARDED_FOR.key());
                    break;
                case 2:
                    ip = request.getHeader(HttpHeader.PROXY_CLIENT_IP.key());
                    break;
                case 3:
                    ip = request.getHeader(HttpHeader.WL_PROXY_CLIENT_IP.key());
                    break;
                case 4:
                    ip = request.getHeader(HttpHeader.HTTP_CLIENT_IP.key());
                    break;
                case 5:
                    ip = request.getHeader(HttpHeader.HTTP_X_FORWARDED_FOR.key());
                    break;
                default:
                    ip = request.getRemoteAddr();
            }
        }

        return ip;
    }

    private static boolean isIpFound(String ip) {
        return ip != null && ip.length() > 0 && !"unknown".equalsIgnoreCase(ip);
    }


}
