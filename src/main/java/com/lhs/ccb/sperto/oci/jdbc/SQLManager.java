package com.lhs.ccb.sperto.oci.jdbc;

import com.lhs.ccb.sperto.oci.jdbc.exceptions.DaoException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.pool.OracleDataSource;

public class SQLManager {
    protected static final Logger logger = Logger.getLogger(SQLManager.class.getName());
    private static Connection connection;

    public SQLManager() {
    }

    public static synchronized void closeConnection() throws DaoException {
        try {
            connection.close();
        } catch (SQLException var1) {
            logger.log(Level.WARNING, "Error during closing Database connection.", var1);
        }

    }

    public static synchronized Connection getConnection() throws DaoException {
        if (connection == null || !isValid(connection)) {
            connection = getNewConnection();
        }

        return connection;
    }

    private static boolean isValid(Connection conn) {
        try {
            Statement s = conn.createStatement();
            s.execute("SELECT 1 FROM DUAL");
            s.close();
            return true;
        } catch (Exception var3) {
            close(conn);
            return false;
        }
    }

    private static synchronized Connection getNewConnection() throws DaoException {
        logger.log(Level.CONFIG, "Getting New Connection...");
        Connection connection = null;

        try {
            OracleDataSource ods = new OracleDataSource();
            ods.setURL("jdbc:oracle:thin:@" + System.getenv("SOISRV_DATABASE_SERVER") + ":" + System.getenv("SOISRV_DATABASE_PORT") + ":" + System.getenv("ORACLE_SID"));
            ods.setUser("SPERTO");
            ods.setPassword("11Mars1996.");
            connection = ods.getConnection();
            connection.setAutoCommit(false);
            return connection;
        } catch (NullPointerException var2) {
            throw new DaoException("The maximum connection in Connection Pool is reached and timeout for waiting new Connection has been reached!");
        } catch (SQLException var3) {
            throw new DaoException("Error during initializing Database Connection.", var3);
        }
    }

    public static void close(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException var2) {
            logger.log(Level.SEVERE, "Error during closing Connection.", var2);
        }

    }

    public static void close(Statement stmt) {
        try {
            if (stmt != null) {
                stmt.close();
            }
        } catch (SQLException var2) {
            logger.log(Level.SEVERE, "Error during closing Statement.", var2);
        }

    }

    public static void close(ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException var2) {
            logger.log(Level.SEVERE, "Error during closing ResultSet.", var2);
        }

    }
}