package com.lhs.ccb.sperto.oci.jdbc.dao;

import com.lhs.ccb.sperto.oci.jdbc.exceptions.DaoException;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CheckUserIpDao extends DAO {

    private static CheckUserIpDao INSTANCE;

    public static CheckUserIpDao getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CheckUserIpDao();
        }
        return INSTANCE;
    }

    private CheckUserIpDao() {
    }

    public void insertLoginTrace(String ip, String hostname, String user, String sessionId, String remoteUser) throws SQLException, DaoException {
        String login = "Login";
        ResultSet rs = this.findResultSetByDynamicSelect("select * from SPERTO.CX_USER_TRACE where sessionid = ? and login = ? and workflow_step = ?", new String[]{sessionId, user, login});
        if (!rs.next()) {
            this.insertUserTrace(ip, hostname, user, sessionId, login, remoteUser);
        }
    }

    public void insertUserTrace(String ip, String hostname, String user, String sessionId, String workflowStep, String remoteUser) throws SQLException, DaoException {
        this.insertOrUpdate("insert into SPERTO.CX_USER_TRACE(LOGIN,IP,HOSTNAME,LOGINDATE,SESSIONID,WORKFLOW_STEP,REMOTE_USER) values (?,?,?,sysdate,?,?,?)", new String[]{user, ip, hostname, sessionId, workflowStep, remoteUser});
        this.commit();
    }
}