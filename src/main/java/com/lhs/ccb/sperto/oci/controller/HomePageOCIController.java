package com.lhs.ccb.sperto.oci.controller;

import com.lhs.ccb.cfw.cda.session.SessionManager;
import com.lhs.ccb.cfw.cda.utility.Log;
import com.lhs.ccb.cfw.sgu.solutionunits.home.HomePageController;
import com.lhs.ccb.cfw.wcs.tagutils.HttpRequestUtils;
import com.lhs.ccb.sperto.oci.utils.HttpServletRequestUtils;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class HomePageOCIController extends HomePageController {

    protected static final Logger logger = Logger.getLogger(HomePageOCIController.class.getName());
    private static final String UNKNOWN = "unknown";

    public HomePageOCIController() {
    }

    public String processPage(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse) {
        super.processPage(paramHttpServletRequest, paramHttpServletResponse);
        SessionManager.logout(paramHttpServletRequest);
        String url = HttpRequestUtils.getBaseURL(paramHttpServletRequest) + "login";
        String str = paramHttpServletResponse.encodeRedirectURL(url);

        try {
            paramHttpServletResponse.sendRedirect(str);
            return "FW_StopProcessing";
        } catch (Exception var6) {
            Log.CFWLogger.log(Level.SEVERE, "Return to login screen failed.", var6);
            HttpRequestUtils.prepareReturnFromNestedSU(paramHttpServletRequest);
            return null;
        }
    }

    public void preparePage(HttpServletRequest paramHttpServletRequest) {

        String userName = HttpServletRequestUtils.getLoginRequestInformationAndStore(paramHttpServletRequest);

        if (userName.equals("ADMX")) {
            paramHttpServletRequest.setAttribute("FW_SetSessionInvalid", "INVALID");
        }

        super.preparePage(paramHttpServletRequest);
    }


}
