package com.lhs.ccb.sperto.oci.jdbc.dao;

import com.lhs.ccb.sperto.oci.jdbc.SQLManager;
import com.lhs.ccb.sperto.oci.jdbc.exceptions.DaoException;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAO {
    protected static final Logger logger = Logger.getLogger(DAO.class.getName());
    protected Connection userConn;

    {
        try {
            userConn = SQLManager.getConnection();
        } catch (DaoException ex) {
            logger.log(Level.SEVERE, ex.getMessage());
        }
    }

    protected boolean logOnFail = false;
    protected FileWriter logOnFailFW = null;
    protected int maxRows;

    public DAO() {
    }

    public void setMaxRows(int maxRows) {
        this.maxRows = maxRows;
    }

    public int getMaxRows() {
        return this.maxRows;
    }

    public ResultSet findResultSetByDynamicSelect(String sql, Object[] sqlParams) throws DaoException {
        boolean isConnSupplied = this.userConn != null;
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = isConnSupplied ? this.userConn : SQLManager.getConnection();
            stmt = conn.prepareStatement(sql);
            stmt.setMaxRows(this.maxRows);

            for (int i = 0; sqlParams != null && i < sqlParams.length; ++i) {
                stmt.setObject(i + 1, sqlParams[i]);
            }

            ResultSet var13 = stmt.executeQuery();
            return var13;
        } catch (Exception var11) {
            logger.log(Level.SEVERE, var11.getMessage(), var11);
            throw new DaoException("Exception: " + var11.getMessage(), var11);
        } finally {
            if (!isConnSupplied) {
                SQLManager.close(conn);
            }

        }
    }

    public int insertOrUpdate(String SQL_INSERT, Object[] sqlParams) throws DaoException {
        boolean isConnSupplied = this.userConn != null;
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = isConnSupplied ? this.userConn : SQLManager.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT);

            int i;
            for (i = 0; sqlParams != null && i < sqlParams.length; ++i) {
                stmt.setObject(i + 1, sqlParams[i]);
            }

            i = stmt.executeUpdate();
            return i;
        } catch (Exception var10) {
            logger.log(Level.SEVERE, var10.getMessage(), var10);
            throw new DaoException("Exception: " + var10.getMessage(), var10);
        } finally {
            SQLManager.close(stmt);
            if (!isConnSupplied) {
                SQLManager.close(conn);
            }

        }
    }

    public void commit() throws DaoException {
        logger.log(Level.CONFIG, "Committing...");

        try {
            this.userConn.commit();
        } catch (SQLException var2) {
            throw new DaoException("Error during Commit", var2);
        }

        logger.log(Level.CONFIG, "Committed");
    }

    public void rollback() {
        logger.log(Level.CONFIG, "Rollbacking...");

        try {
            this.userConn.rollback();
        } catch (SQLException var2) {
            logger.log(Level.SEVERE, "Error during Rollback", var2);
        }

        logger.log(Level.CONFIG, "Rollbacked");
    }
}
