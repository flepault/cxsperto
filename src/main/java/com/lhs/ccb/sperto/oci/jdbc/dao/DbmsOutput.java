package com.lhs.ccb.sperto.oci.jdbc.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

class DbmsOutput {
    private CallableStatement enable_stmt;
    private CallableStatement disable_stmt;
    private CallableStatement show_stmt;

    public DbmsOutput(Connection conn) throws SQLException {
        this.enable_stmt = conn.prepareCall("begin dbms_output.enable(:1); end;");
        this.disable_stmt = conn.prepareCall("begin dbms_output.disable; end;");
        this.show_stmt = conn.prepareCall("declare  l_line varchar2(255);  l_done number;  l_buffer long; begin  loop  exit when length(l_buffer)+255 > :maxbytes OR l_done = 1;  dbms_output.get_line( l_line, l_done);  l_buffer := l_buffer || l_line || chr(10);  end loop;  :done := l_done;  :buffer := l_buffer; end;");
    }

    public void enable(int size) throws SQLException {
        this.enable_stmt.setInt(1, size);
        this.enable_stmt.executeUpdate();
    }

    public void disable() throws SQLException {
        this.disable_stmt.executeUpdate();
    }

    public StringBuffer show() throws SQLException {
        StringBuffer dbms = new StringBuffer("");
        this.show_stmt.registerOutParameter(2, 4);
        this.show_stmt.registerOutParameter(3, 12);

        do {
            this.show_stmt.setInt(1, 32000);
            this.show_stmt.executeUpdate();
            dbms.append(this.show_stmt.getString(3));
        } while(this.show_stmt.getInt(2) != 1);

        return dbms;
    }

    public void close() throws SQLException {
        this.enable_stmt.close();
        this.disable_stmt.close();
        this.show_stmt.close();
    }
}
